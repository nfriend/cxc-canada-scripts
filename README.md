# :warning: This project is deprecated! :warning:

Since the creation of the [GitLab Canada Corp](https://about.gitlab.com/handbook/benefits/canada-corp-benefits/), we no longer need to submit timesheets :tada:

# cxc-canada-scripts 🇨🇦

This repository contains a Google Sheet and script that automates the population and submission of weekly CXC Canada timesheets. This script is heavily based on [Cynthia Ng's excellent snippet](https://gitlab.com/snippets/1783173). At a high level, this script does the following:

1. Opens your timesheet in Google Sheets.
1. Edits the "Week Ending" date field to be the upcoming Sunday.
1. Updates each row in the main time grid:
   - For normal working days, the script inserts 8 hours with no description.
   - For stat holidays, the script inserts 0 hours and adds a description with the holiday name. It also sets the Type to "Statutory Holiday".
   - For PTO, the script inserts 8 hours with the description "PTO" and Type "Vacation".
1. Updates the signature date to today.
1. Saves the spreadsheet and renders it as a PDF.
1. Emails the PDF as an attachment to your manager for approval and CC's CXC Payroll.

A completed PDF timesheet looks something like this:

![Screenshot of a completed timesheet](./assets/timesheet_screenshot.png)

## Setup

To set this Google Sheets script up for yourself:

1. Download the timesheet template from this project and upload to your Google Sheets.
![Importing Excel to Sheets](./assets/import_excel_to_sheets.gif)
    1. Open a new tab in your browser and navigate to [sheet.new](http://sheet.new)
    1. Goto `File` → `Import`.
    1. Select `Upload`, then Select a file from your device.
    1. Choose the Excel spreadsheet you saved earlier.
    1. After upload, choose the default of `Replace spreadsheet` then click `Import data`.
    1. After import, click the current title of `Untitled spreadsheet` and update the name to something more memorable like `CXC Timesheet`. Then click outside that cell to save.
1. Fill in all the details that aren't automatically updated by the script.
   - Consultant Name, Location Site, Name.
1. Update the spreadsheet's timezone to GMT: (This allows the script to accurately deal with time calculations)
   - `File` → `Spreadsheet settings` → `General` → Time zone → GMT (no daylight saving).
1. Open up the script editor via `Tools` → `Script editor`.
1. Update the script's timezone to GMT:
   - File → Project properties → Info → Time zone → GMT (no daylight saving).
1. Paste the contents of [`submitTimesheet.js`](./submitTimesheet.js) into the editor.
1. Update all data in the `config` variable at the top of the file with your own info.
   - Make sure all `<MANAGER_USERNAME>`, `<USERNAME>`, and `<FULL_NAME>` fields are updated.
   - Make sure you double check which holidays are considered stat holidays - they vary from province to province.
   - Keep your PTO updated to ensure proper reporting on your paystub.
1. Verify that the script is working:
   - In the "Select function" dropdown in the menu bar, select `main`. Then, click the "play" button.

After following these steps, you should receive an email with a short message and a completed timesheet attached. (Note that the date is fixed to 2019-01-18 when `debug === true`.). Carefully read the email and the attached PDF to verify the output is correct. When you're confident everything looks good, flip line 7 to `debug: false`.

### Running the script every week

To run the script automatically every Friday:

1. Open the script you just created in the script editor and select Edit → Current project's triggers.
1. Select "Add Trigger" in the bottom-right corner of the screen.
1. Update the "Select event source" dropdown to "Time-driven".
1. Update the "Select type of time based trigger" dropdown to "Week timer".
1. Update the "Select day of week" dropdown to "Every Friday".
1. Update the "Select time of day" dropdown to the time you'd like your timesheet to be sent. (I choose 2:00 PM.)
1. Click "Save".

## Making it look pretty

There's a few changes you can make to the CXC Google Sheets template to make it a little easier on the eyes:

1. Ensure all comments are removed. Comments are appear as "[1]" in the rendered PDF.
1. Remove and re-add borders. Some extra borders show up when using the original template (do a test run to see for yourself).
1. Add an image of your signature. This is totally optional (the signature isn't required), but it makes it look official :smile:.

## Notes

The [`submitTimesheet.js`](./submitTimesheet.js) script is heavily dependent on the structure of the CXC timesheet template. Any changes to the structure of this template will likely break this script.

The script assumes it is running on a Friday. Running the script on any other day will result in an error (unless `config.debug === true`).

If any errors occur while executing the script, the script will send you an email alert with the error details. The details of the error email are specified via `config.errorEmail`.

In its current form, PTO days and holidays must be kept up-to-date by manually editing the script.

Make sure to double-check that you are using the correct stat holiday dates! They vary from province to province.
